package util

import (
	"strings"
	"testing"
)

func TestTrimPathInvalidChars(t *testing.T) {
	trimed := TrimPathInvalidChars("ksjadfi*/?adf")
	if strings.Compare(trimed, "ksjadfiadf") != 0 {
		t.Fatalf("trimed: %s\n", trimed)
	}
	return
}
